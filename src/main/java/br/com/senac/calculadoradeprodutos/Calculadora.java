package br.com.senac.calculadoradeprodutos;

public class Calculadora {

    public double calcula(Produto produto, FormaPagamento formaPagamento) {

        switch (formaPagamento) {
            case AVISTA_DINHEIRO:
                return produto.getValor() * 0.9; 
            case AVISTA_CARTAO:
                return produto.getValor() * 0.95;
            case DUAS_VEZES_CARTAO:
                return produto.getValor();
            default:
                return produto.getValor() * 1.1;
        }

    }

}
