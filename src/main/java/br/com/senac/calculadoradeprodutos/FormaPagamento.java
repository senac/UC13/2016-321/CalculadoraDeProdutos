
package br.com.senac.calculadoradeprodutos;


public enum FormaPagamento {
    AVISTA_DINHEIRO , 
    AVISTA_CARTAO, 
    DUAS_VEZES_CARTAO , 
    TRES_VEZES_CARTAO 
}
