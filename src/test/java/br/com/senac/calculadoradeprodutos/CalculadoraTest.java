package br.com.senac.calculadoradeprodutos;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculadoraTest {

    @Test
    public void deveCalcularDescontoDe10PorCentoParaPagamentoAvistaEmDinheiro() {
        Produto produto = new Produto(1, "Sabao", 10);
        Calculadora calculadora = new Calculadora();
        double preco = calculadora.calcula(produto, FormaPagamento.AVISTA_DINHEIRO);
        assertEquals(9, preco, 0.001);
    }

    @Test
    public void deveCalcularDescontoDe5PorCentoParaPagamentoAvistaEmCartao() {
        Produto produto = new Produto(1, "Sabao", 10);
        Calculadora calculadora = new Calculadora();
        double preco = calculadora.calcula(produto, FormaPagamento.AVISTA_CARTAO);
        assertEquals(9.5, preco, 0.001);
    }
    
     @Test
    public void deveCalcularSemDescontoParaPagamento2VezesEmCartao() {
        Produto produto = new Produto(1, "Sabao", 10);
        Calculadora calculadora = new Calculadora();
        double preco = calculadora.calcula(produto, FormaPagamento.DUAS_VEZES_CARTAO);
        assertEquals(10, preco, 0.001);
    }
    
     @Test
    public void deveCalcularComAcresimo10PorCentoParaPagamento3VezesEmCartao() {
        Produto produto = new Produto(1, "Sabao", 10);
        Calculadora calculadora = new Calculadora();
        double preco = calculadora.calcula(produto, FormaPagamento.TRES_VEZES_CARTAO);
        assertEquals(11, preco, 0.001);
    }
    

}
